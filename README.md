**Задание 1**
Определить класс с методами (они могут быть статическими или не статическими, в последнем случае использовать ООП подход),
которые преобразовывают входную информацию в выходную. В качестве входной информации (input data) использовать текст
следующей структуры (значения Login/Name/Email в общем случае могут быть любыми):
Login;Name;Email
ivanov;Ivan Ivanov;ivanov@mail.ru
petrov;Petr Petrov;petrov@google.com
obama;Barack Obama;obama@google.com
bush;George  Bush;bush@mail.ru

**1.1. Метод convert1**
Должен преобразовывать input data в строку следующего вида:
ivanov==>ivanov@mail.ru
petrov==>petrov@google.com
obama==>obama@google.com
bush==>bush@mail.ru

**1.2. Метод convert2**
Должен преобразовывать input data в строку следующего вида:
Ivanov Ivan(email:ivanov@mail.ru)
Petrov Petr(email:petrov@google.com)
Obama Barack(email:obama@google.com)
Bush George(email:bush@mail.ru)

**1.3. Метод convert3**
Должен преобразовывать input data в строку следующего вида (почтовый домен ==> список логинов через запятую тех
пользователей, чьи почтовые ящики зарегестрированны в данном домене):
mail.ru==>ivanov,bush
google.com==>petrov,obama

**1.4. Метод convert4**
Должен преобразовывать input data в строку следующего вида (должна быть добавлена колонка Password, сам пароль должен
состоять ровно из 4 цифр, которые генерируются случайным образом):
Login;Name;Email;Password
ivanov;Ivan Ivanov;ivanov@mail.ru;2344
petrov;Petr Petrov;petrov@google.com;3423
obama;Barack Obama;obama@google.com;6554
bush;George Bush;bush@mail.ru;4534

**Задание 2**
Дан текст. Найти и напечатать все слова максимальной и все слова минимальной длины. Словом считать последовательность
содержащую только буквы (все остальные символы в состав слова не входят).

**Input data**
When I was younger, so much younger than today
I never needed anybody's help in any way
But now these days are gone,I'm not  so self-assured
Now I find I've changed my mind
I've opened up the doors

**Output data**
Min: I, s, m
Max: younger, anybody, assured, changed

**Задание 3**
Дан текст. Напечатать текст, поставив первый символ каждого слова в верхний регистр.

**Input data**
When I was younger
I never needed

**Output data**
When I Was Younger
I Never Needed

**Задание 4**
Для хеширования информации (например, паролей) используют метод MessageDigest#digest, который возвращает хеш в виде
массива байт.

Написать статический метод, который на вход принимает два параметра: (1) строку, хеш которой нужно получить; (2) названия
алгоритма хеширования. Выход должен представлять из себя строку из шестнадцатеричных цифр: каждому байту соответствует
две шестнадцатеричные цифры. Например, если некоторый элемент массива байт равен -29, то в двоичном разложении он имеет
вид 1110_0011 и ему соответствует пара E3.

**Задание 5**
Создать класс с двумя статическими методами перевода из десятичной системы счисления в римскую и обратно.
Рабочий диапазон методов - от 1 до 100 включительно.




