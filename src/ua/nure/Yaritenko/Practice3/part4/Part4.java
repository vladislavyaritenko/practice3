package ua.nure.Yaritenko.Practice3.part4;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Part4 {
    private static String hash(String input, String algorithm) throws NoSuchAlgorithmException {
        MessageDigest digest = MessageDigest.getInstance(algorithm);
        byte[] hash = digest.digest(input.getBytes(StandardCharsets.UTF_8));
        StringBuilder hexString = new StringBuilder();
        StringBuilder hexString2 = new StringBuilder();
        String str;
        for (byte b : hash) {
            str = Integer.toBinaryString(b);
            if (b < 0) {
                String strBit1 = str.substring(str.length() - 8, str.length() - 4);
                int i = Integer.valueOf(strBit1, 2);
                String strBit2 = str.substring(str.length() - 4, str.length());
                int i2 = Integer.valueOf(strBit2, 2);
                hexString.append(Integer.toHexString(i).toUpperCase()).append(Integer.toHexString(i2).toUpperCase()).append("  ");
            } else {
                StringBuilder sb2 = new StringBuilder();
                if (str.length() - 1 <= 8) {
                    int i = str.length() - 1;
                    while (i != 7) {
                        sb2.append("0");
                        i++;
                    }
                }
                sb2.append(str).append(" ");
                sb2.deleteCharAt(sb2.length() - 1);
                str = sb2.toString();
                String strBit1 = str.substring(str.length() - 8, str.length() - 4);
                int i = Integer.valueOf(strBit1, 2);
                String strBit2 = str.substring(str.length() - 4, str.length());
                int i2 = Integer.valueOf(strBit2, 2);
                hexString.append(Integer.toHexString(i).toUpperCase()).append(Integer.toHexString(i2).toUpperCase()).append("  ");
            }
        }
        hexString2.append(hexString).append(System.lineSeparator());
        return hexString2.toString();
    }

    public static void main(String[] args) throws NoSuchAlgorithmException {
        System.out.println("===Part 4");
        System.out.println(hash("password", "SHA-256"));
        System.out.println(hash("passwort", "SHA-256"));
    }
}
