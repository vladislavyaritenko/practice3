package ua.nure.Yaritenko.Practice3.part1;

import ua.nure.Yaritenko.Practice3.Util;

import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part1 {
    public static final String EOL = System.lineSeparator();

    public static String convert1(String input) {
        StringBuilder strOut = new StringBuilder();
        Pattern pattern = Pattern.compile("(?um)^(\\w+)(;)(\\S+)(\\s+)(\\S+)(;)((\\w+)(@?)(\\w+\\.\\w+))$");
        Matcher matcher = pattern.matcher(input);
        while (matcher.find()) {
            strOut = strOut.append(matcher.group(1)).append(" ==> ").append(matcher.group(7)).append(EOL);
        }
        return strOut.toString();
    }

    public static String convert2(String input) {
        StringBuilder strOut = new StringBuilder();
        Pattern pattern = Pattern.compile("(?um)^(\\w+)(;)(\\S+)(\\s)(\\S+)(;)(.*)$");
        Matcher matcher = pattern.matcher(input);
        while (matcher.find()) {
            strOut = strOut.append(matcher.group(5)).append(" ").append(matcher.group(3)).append(" ").append("(email: ").append(matcher.group(7)).append(")").append(EOL);
        }
        return strOut.toString();
    }

    public static String convert3(String input) {
        StringBuilder strOut = new StringBuilder();
        StringBuilder strOut2 = new StringBuilder();
        Pattern pattern1;
        Matcher matcher1;
        int offset = 0;
        pattern1 = Pattern.compile("(?um)^(\\w+)(;)(\\S+)(\\s)(\\S+)(;)(\\w+)(@?)(\\w+\\.\\w+)$");
        matcher1 = pattern1.matcher(input);
        ArrayList<String> al = new ArrayList<>();
        while (matcher1.find()) {
            if (!(al.contains(matcher1.group(9)))) {
                al.add(matcher1.group(9));
            }
            strOut = strOut.append(matcher1.group(7)).append(matcher1.group(8)).append(matcher1.group(9)).append(EOL);
        }
        Pattern pattern2 = Pattern.compile("(?um)^(\\w+)(@?)(\\w+\\.?\\w+)$");
        Matcher matcher2 = pattern2.matcher(strOut);
        for (String a : al) {
            strOut2 = strOut2.append(a).append(" ==> ");
            offset = 0;
            while (matcher2.find(offset)) {
                offset = matcher2.end();
                if (a.equals(matcher2.group(3))) {
                    strOut2 = strOut2.append(matcher2.group(1)).append(", ");
                }
            }
            strOut2 = strOut2.deleteCharAt((strOut2.length() - 2));
            strOut2 = strOut2.append(EOL);
        }
        return strOut2.toString();
    }

    public static String convert4(String input) {
        StringBuilder strOut = new StringBuilder();
        Random random = new Random();
        Pattern pattern = Pattern.compile("(?um)^(\\S?)(\\w+)(;)(\\S?)(\\w+)(;)(\\S?)(\\w+)$");
        Matcher matcher = pattern.matcher(input);
        Pattern pattern1 = Pattern.compile("(?um)^(\\w+)(;)(\\S+)(\\s?)(\\S+)(;)((\\w+)(@?)(\\w+\\.\\w+))$");
        Matcher matcher1 = pattern1.matcher(input);
        while (matcher.find()) {
             strOut.append(matcher.group());
        }
        strOut.append(";Password").append(EOL);
        while (matcher1.find()) {
            strOut.append(matcher1.group()).append(";").append(random.nextInt(10)).append(random.nextInt(10)).append(random.nextInt(10)).append(random.nextInt(10)).append(EOL);

        }

        return strOut.toString();
    }

    public static void main(String[] args) {
        String input = Util.getInput("part1.txt");
        System.out.println(EOL + "===Part 1.1");
        System.out.println(Part1.convert1(input));

        System.out.println("===Part 1.2");
        System.out.println(Part1.convert2(input));

        System.out.println("===Part 1.3");
        System.out.println(Part1.convert3(input));

        System.out.println("===Part 1.4");
        System.out.println(Part1.convert4(input));


    }

}
