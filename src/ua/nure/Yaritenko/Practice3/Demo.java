package ua.nure.Yaritenko.Practice3;

import ua.nure.Yaritenko.Practice3.part1.Part1;
import ua.nure.Yaritenko.Practice3.part2.Part2;
import ua.nure.Yaritenko.Practice3.part3.Part3;
import ua.nure.Yaritenko.Practice3.part4.Part4;
import ua.nure.Yaritenko.Practice3.part5.Part5;

import java.security.NoSuchAlgorithmException;

public class Demo {
    public static void main(String[] args) throws NoSuchAlgorithmException {
        Part1.main(new String[]{});
        Part2.main(new String[]{});
        Part3.main(new String[]{});
        Part4.main(new String[]{});
        Part5.main(new String[]{});
    }
}
