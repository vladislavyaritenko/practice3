package ua.nure.Yaritenko.Practice3.part2;

import ua.nure.Yaritenko.Practice3.Util;


import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Part2 {
    public static final String EOL = System.lineSeparator();
    public static String convert1(String input){
        StringBuilder strOut = new StringBuilder("Min: ");
        ArrayList<String> alMin = new ArrayList<>();
        ArrayList<String> alMax = new ArrayList<>();
        int Min = Integer.MAX_VALUE, Max = 0;
        Pattern pattern1 = Pattern.compile("(?uim)(([a-zа-яёє])*)");
        Matcher matcher = pattern1.matcher(input);
        Matcher matcher1 = pattern1.matcher(input);
        while(matcher.find()) {
            if((Min > matcher.group().length() & (matcher.group().length() != 0))){
                Min = matcher.group().length();
            }
            if(Max < matcher.group().length())
                Max = matcher.group().length();
        }
        while(matcher1.find()){
            if(Min == matcher1.group().length() ){
                if (!(alMin.contains(matcher1.group()))) {
                    alMin.add(matcher1.group());
                }
            }
            if(Max == matcher1.group().length()){
                if (!(alMax.contains(matcher1.group()))) {
                    alMax.add(matcher1.group());
                }
            }
        }
        for(String str : alMin){
            strOut.append(str).append(", ");
        }
        strOut.deleteCharAt(strOut.length() - 2);
        strOut.append(EOL).append("Max: ");
        for(String str : alMax){
            strOut.append(str).append(", ");
        }
        strOut.deleteCharAt(strOut.length() - 2);
        return strOut.toString();
    }


    public static void main(String[] args) {
        String input = Util.getInput("part2.txt");
        System.out.println(EOL + "===Part2");
        System.out.println(Part2.convert1(input));

    }
}
