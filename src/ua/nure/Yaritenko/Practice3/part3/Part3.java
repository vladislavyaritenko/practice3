package ua.nure.Yaritenko.Practice3.part3;

import ua.nure.Yaritenko.Practice3.Util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Part3 {
    private static final String EOL = System.lineSeparator();
    private static String convert1(String input) {
        StringBuilder strOut = new StringBuilder();
        Pattern pattern = Pattern.compile("(?umi)^((([a-zа-яёє])+)(\\s?))+$");
        Matcher matcher = pattern.matcher(input);
        Pattern pattern1 = Pattern.compile("(?umi)([a-zа-яёє])+");
        Matcher matcher1;
        String str;
        while (matcher.find()) {
            str = matcher.group();
            matcher1 = pattern1.matcher(str);
            while(matcher1.find()) {
                if (matcher1.group().length() == 1) {
                    strOut.append(matcher1.group().toUpperCase()).append(" ");
                } else {
                    strOut.append(matcher1.group().substring(0, 1).toUpperCase()).append(matcher1.group().substring(1)).append(" ");
                }
            }
            strOut.append(EOL);
        }
        return strOut.toString();
    }

    public static void main(String[] args) {
        String input = Util.getInput("part3.txt");
        System.out.println(EOL + "===Part3");
        System.out.println(Part3.convert1(input));

    }
}
