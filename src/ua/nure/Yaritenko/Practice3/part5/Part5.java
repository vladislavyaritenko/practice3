package ua.nure.Yaritenko.Practice3.part5;


import java.util.TreeMap;

public class Part5 {


    private static String decimal2Roman(int x) {
        TreeMap<Integer, String> tr = new TreeMap<>();
        tr.put(100, "C");
        tr.put(90, "XC");
        tr.put(50, "L");
        tr.put(40, "XL");
        tr.put(10, "X");
        tr.put(9, "IX");
        tr.put(5, "V");
        tr.put(4, "IV");
        tr.put(1, "I");
        int l = tr.floorKey(x);
        if (x == l) {
            return tr.get(x);
        }
        return tr.get(l) + decimal2Roman(x - l);
    }


    private static int roman2Decimal(String s) {
        TreeMap<Character, Integer> tr = new TreeMap<>();
        tr.put('I', 1);
        tr.put('X', 10);
        tr.put('C', 100);
        tr.put('V', 5);
        tr.put('L', 50);
        int intNum = 0;
        int prev = 0;
        for (int i = s.length() - 1; i >= 0; i--) {
            int temp = tr.get(s.charAt(i));
            if (temp < prev) {
                intNum -= temp;
            } else {
                intNum += temp;
            }
            prev = temp;
        }
        return intNum;
    }

    public static void main(String[] args) {
        System.out.println("===Part 5");
        for (int i = 1; i <= 100; i++) {
            System.out.println(i + " ===> " + Part5.decimal2Roman(i) + " ===> " + Part5.roman2Decimal(Part5.decimal2Roman(i)));
        }
    }
}
